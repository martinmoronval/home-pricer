class TransactionsController < ApplicationController
  def fetch
    @zip_code = params.require(:zip_code)
    @resp = JSON.parse(Typhoeus.get("http://api.cquest.org/dvf?code_postal=#{@zip_code}").response_body)['resultats']
  end
end
